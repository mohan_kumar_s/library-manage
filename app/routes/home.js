import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  store: service(),
  authenticate: service('authentication'),
  actions:{
    logout:function(){
      this.authenticate.clear()
    }
  },

  model() {
    return RSVP.hash({
      auth: this.authenticate,
      user: this.store.query('home',{
        filter:{
          username: this.authenticate.uname
        }
      })
    });

  }
  
});
