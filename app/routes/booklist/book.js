import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
    authenticate:service('authentication'),
    store: service(),
    actions:{
        back: function() {
            this.replaceWith('booklist')
        }
    },

    model(){
        
    }
});
