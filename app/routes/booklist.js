import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  
  store: service(),
  authenticate: service('authentication'),
  actions:{
  
    addbook: function(user,book,log){

      let today= new Date()
      let takendate = today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear()
      let due = new Date(today);
      due.setDate(due.getDate() + 30);
      let duedate = due.getDate()+'/'+(due.getMonth()+1)+'/'+due.getFullYear()
      let array ={
        title: book.get('title'),
        author: book.get('author'),
        publication: book.get('publication'),
        year: book.get('year'),
        category: book.get('category'),
        takendate: takendate,
        duedate: duedate
      }
  
      book.set('quantity',book.get('quantity')-1)
      user.get('books').push(array)
      user.get('history').push('Book '+array.title+' taken on '+array.takendate)
      this.store.findAll('librarylog').then(function(log) {
        log.forEach(element => {
            element.get('logs').push('Book '+array.title+' taken on '+array.takendate+' by '+user.get('name'))
            element.save()
        });
      });
      
      book.get('users').push(user.get('name'))
      book.save()
      user.save()
    },

    removebook:  function(book){
      book.destroyRecord();
    }
  },

  model() {
    return RSVP.hash({
        user: this.store.query('home',{
            filter:{
              username: this.authenticate.uname
            }
          }),
        books: this.get('store').findAll('books'),
        auth: this.authenticate,
    });

  }
  
});

