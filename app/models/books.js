import Model from '@ember-data/model';
import DS from 'ember-data';

export default Model.extend({

    users: DS.attr(),
    title: DS.attr(),
    author: DS.attr(),
    publication: DS.attr(),
    year: DS.attr(),
    category: DS.attr(),
    quantity: DS.attr('number',{defaultValue : 10}),
});
