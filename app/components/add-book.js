import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    store: service(),
    isaddclicked: false,

    actions:{
        newbook: function(){
            var title = document.getElementById('title')
            var author = document.getElementById('author')
            var publication = document.getElementById('publication')
            var year = document.getElementById('year')
            var category
            if(document.getElementById('technical').checked){category = 'Technical'}
            if(document.getElementById('reference').checked){category = 'Reference'}
            if(document.getElementById('journal').checked){category = 'Journal'}
            if(document.getElementById('special').checked){category = 'Special'}
            var book = this.store.createRecord('books',{
              users: [],
              title: title.value,
              author: author.value,
              publication: publication.value,
              year: year.value,
              category: category
            })
            book.save()
          },
        addclick: function(){
            this.toggleProperty('isaddclicked')
          },
    }
    
});
