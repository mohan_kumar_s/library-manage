import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  

  store: service(),
  authenticate: service('authentication'),
  actions:{

    removeBook: function(user,book,index,log){
      let today = new Date()
      let dd = today.getDate();
      let mm = today.getMonth()+1;
      let yyyy = today.getFullYear();
      let date = dd+'/'+mm+'/'+yyyy
      user.get('books').splice(index,1)
      user.get('history').push('Book '+book+' returned on '+date)
      this.store.findAll('librarylog').then(function(log) {
        log.forEach(element => {
            element.get('logs').push('Book '+book+' returned on '+date+' by '+user.get('name'))
            element.save()
        });
      });
      user.save()
      this.store.query('books', { filter: { title: book } }).then(function(book) {
        book.forEach(element => {
            element.set('quantity',element.get('quantity')+1)
            element.get('users').splice(user.get('name'))
            element.save()
        });
    });
    this.model()
    },
  },


  model() {

    return RSVP.hash({
        user: this.store.query('home',{
            filter:{
              username: this.authenticate.uname
            }
          }),
        books: this.get('store').findAll('books'),
        auth: this.authenticate,

    });

  }
  
});

