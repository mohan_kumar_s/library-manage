'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'lib-manage',
    environment,

    rootURL: '/',
    locationType: 'auto',

    firebase: {
      apiKey: "AIzaSyC5KNrALIBlmXSObsLAz36__8MdDioW0nE",
      authDomain: "lib-manage-d34de.firebaseapp.com",
      databaseURL: "https://lib-manage-d34de.firebaseio.com",
      projectId: "lib-manage-d34de",
      storageBucket: "lib-manage-d34de.appspot.com",
      messagingSenderId: "107448927111",
      appId: "1:107448927111:web:732eb89310b42f1a3e4922",
      measurementId: "G-H4L9ZKEB53"
    },
    contentSecurityPolicy: {
      'script-src': "'self' 'unsafe-eval' apis.google.com",
      'frame-src': "'self' https://*.firebaseapp.com",
      'connect-src': "'self' wss://*.firebaseio.com https://*.googleapis.com"
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
