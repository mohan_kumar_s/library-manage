import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';


export default Route.extend({  
  authenticate:service('authentication'),
  actions:{
    getin: function(){
      let username= document.getElementById('username')
      let password=document.getElementById('password')
      this.authenticate.newUser(username.value, password.value)
      document.getElementById('username').value=null
      document.getElementById('password').value=null
      this.transitionTo('home') 
    }
  },
  model(){
    return {user: this.authenticate}
  }
    
});
