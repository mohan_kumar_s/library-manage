import Service from '@ember/service';
import { inject as service } from '@ember/service';
import {computed} from '@ember/object';
export default Service.extend({
    store:service(),

    uname:'',
    password:'',
    isSignedIn:false,
    info: computed('uname','password',function(){
        return this.store.query('home',{
            filter :{
                name: this.uname,
                password: this.password
            }
        })
    }),
    newUser(name,pass){
       
        this.set('uname',name)
        this.set('password',pass)
        this.set('isSignedIn',true)
    },
    clear(){
        this.set('uname',null)
        this.set('password',null)
        this.set('isSignedIn',false)
        this.set('info',null)
    }
    
});
