import Route from '@ember/routing/route';

export default Route.extend({
    actions: {
        signup: function() {
            const name = document.getElementById('name');
            const dob = document.getElementById('dob');
            if(document.getElementById('male').checked){
                var gender = 'Male'
            }
            else{
                var gender = 'female'
            }
            const contact = document.getElementById('contact');
            const username = document.getElementById('username');
            const password = document.getElementById('password');
            var user = this.store.createRecord('home',{
                books: [],
                name: name.value,
                dob: dob.value,
                gender: gender,
                contact: contact.value,
                username: username.value,
                password: password.value,
                history: []
            });
            user.save();
            document.getElementById('name').value=null
            document.getElementById('dob').value=null
            document.getElementById('contact').value=null
            document.getElementById('username').value=null
            document.getElementById('password').value=null
            this.transitionTo('login')
        }
      },
});
