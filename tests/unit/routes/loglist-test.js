import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | loglist', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:loglist');
    assert.ok(route);
  });
});
