import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | booklist/book', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:booklist/book');
    assert.ok(route);
  });
});
