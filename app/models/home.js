import Model from '@ember-data/model';
import DS from 'ember-data';

export default Model.extend({

    books: DS.attr(),
    name: DS.attr(),
    dob: DS.attr(),
    gender: DS.attr(),
    contact: DS.attr(),
    username: DS.attr(),
    password: DS.attr(),
    isAdmin: DS.attr('boolean',{defaultValue:false}),
    history: DS.attr()
});
