import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

export default Route.extend({
  
  store: service(),
  authenticate: service('authentication'),
  actions:{
    clearlogs: function(user){
      user.set('history',[])
      user.save()
    }
  },


  model() {

    return RSVP.hash({
        user: this.store.findAll('home'),
        logs: this.store.findAll('librarylog'),
        auth: this.authenticate
    });

  }
  
});
