import Component from '@ember/component';

export default Component.extend({
    isExpanded : false,

    actions:{
        expand: function(){
            this.toggleProperty('isExpanded');
        }
    },
});
