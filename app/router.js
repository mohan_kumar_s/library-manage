import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}


Router.map(function() {
  this.route('home', );
  this.route('about');
  this.route('login');
  this.route('signup');
  this.route('offers');
  this.route('contact');
  this.route('booklist', function() {
    this.route('book', { path: '/:book_id' });
  });
  this.route('mybooks');
  this.route('mylog');
  this.route('loglist');
});
